/*
 * Copyright (c) 2022 Huawei Technology Duesseldorf GmbH. 2022. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Contributors:
 *   Huawei Technologies Duesseldorf GmbH
 *   Security Forge
 */

package verifiablecredential

import (
	"encoding/base64"
	"encoding/json"
	"time"
)

type VerifiablePresentation struct {
	Id               string                 `json:"id"`
	Issuer           string                 `json:"issuer"`
	IssuanceDate     time.Time              `json:"issuanceDate"`
	ExpirationDate   time.Time              `json:"expirationDate"`
	Context          []string               `json:"@context"`
	CredType         []string               `json:"type"`
	Vc               []VerifiableCredential `json:"verifiableCredential"`
	PresentationMeta map[string]interface{} `json:"credentialMetadata"`
	Proof            map[string]interface{} `json:"proof"`
}

func InitVP(id, issuer string) *VerifiablePresentation {
	return &VerifiablePresentation{
		Id:               id,
		Issuer:           issuer,
		IssuanceDate:     time.Now(),
		ExpirationDate:   time.Now().Add(100),
		Context:          []string{},
		CredType:         []string{},
		Vc:               []VerifiableCredential{},
		PresentationMeta: make(map[string]interface{}),
		Proof:            make(map[string]interface{}),
	}
}

func (vp *VerifiablePresentation) BuildLD(did string) (*VerifiablePresentation, error) {
	// mocking the DID resolve function
	pubKey, privateKey, err := getSubKey(did)
	if err != nil {
		return vp, err
	}
	_, privKey := decodeKeys(pubKey, privateKey)
	vcJson, err := json.Marshal(vp)
	if err != nil {
		return vp, err
	}
	proofType := vp.Proof["ProofType"].(string)
	// signing the credential payload
	signature, err := signer(proofType, vcJson, privKey)
	if err != nil {
		return vp, err
	}
	vp.Proof["proofValue"] = base64.StdEncoding.EncodeToString(signature)
	vp.Proof["created"] = time.Now()
	return vp, err
}
