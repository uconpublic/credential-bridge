/*
 * Copyright (c) 2022 Huawei Technology Duesseldorf GmbH. 2022. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Contributors:
 *   Huawei Technologies Duesseldorf GmbH
 *   Security Forge
 */

package verifiablecredential

import (
	"crypto/ed25519"
	"encoding/base64"
	"encoding/json"
	"errors"
	"time"
)

var w3cAttr [6]string = [6]string{"id", "issuer", "issuanceDate", "@context", "type", "credentialSubject"}
var gaiaX [2]string = [2]string{"sub", "gx_trust_status"}

type VerifiableCredential struct {
	Id             string   `json:"id"`
	Issuer         string   `json:"issuer"`
	IssuanceDate   string   `json:"issuanceDate"`
	ExpirationDate string   `json:"expirationDate"`
	Context        []string `json:"@context"`
	CredType       string   `json:"type"`

	CredentialSubject map[string]interface{} `json:"credentialSubject"`
	Proof             map[string]interface{} `json:"proof"`
}

func (v *VerifiableCredential) Create(claims string) *VerifiableCredential {
	var result map[string]interface{}
	json.Unmarshal([]byte(claims), &result)

	for _, requiredAttr := range w3cAttr {
		if _, ok := result[requiredAttr]; !ok {
			return nil
		} else if result[requiredAttr] == nil {
			return nil
		}
	}

	v.Id = result["id"].(string)
	v.Issuer = result["issuer"].(string)
	v.IssuanceDate = result["issuanceDate"].(string)
	v.ExpirationDate = result["expirationDate"].(string)
	for _, value := range result["@context"].([]interface{}) {
		v.Context = append(v.Context, value.(string))
	}
	v.CredType = result["type"].(string)

	v.CredentialSubject = make(map[string]interface{}, len(result["credentialSubject"].(map[string]interface{})))
	for k, value := range result["credentialSubject"].(map[string]interface{}) {
		v.CredentialSubject[k] = value
	}

	if _, ok := result["proof"]; ok {
		v.Proof = make(map[string]interface{}, len(result["proof"].(map[string]interface{})))
		for k, value := range result["proof"].(map[string]interface{}) {
			v.Proof[k] = value
		}
	}

	return v
}

func (v *VerifiableCredential) BuildJWT(did string) (string, error) {
	pubKey, privateKey, err := getSubKey(did)
	if err != nil {
		return "", err
	}
	_, privKey := decodeKeys(pubKey, privateKey)
	jwtHeader := map[string]string{"typ": "JWT", "alg": v.Proof["proofType"].(string), "kid": v.Proof["verificationMethod"].(string)}
	headerJson, err := json.Marshal(jwtHeader)
	if err != nil {
		return "", err
	}
	vcJson, err := json.Marshal(v)
	if err != nil {
		return "", err
	}

	jwtBody := base64.StdEncoding.EncodeToString(headerJson) + "." + base64.StdEncoding.EncodeToString(vcJson)
	signature, err := signer(v.Proof["proofType"].(string), []byte(jwtBody), privKey)
	if err != nil {
		return "", err
	}
	return jwtBody + "." + base64.StdEncoding.EncodeToString(signature), nil
}

func (v *VerifiableCredential) BuildLD(did string) (*VerifiableCredential, error) {
	pubKey, privateKey, err := getSubKey(did)
	if err != nil {
		return v, err
	}
	_, privKey := decodeKeys(pubKey, privateKey)
	vcJson, err := json.Marshal(v)
	if err != nil {
		return v, err
	}
	signature, err := signer(v.Proof["proofType"].(string), vcJson, privKey)
	if err != nil {
		return v, err
	}
	v.Proof["proofValue"] = base64.StdEncoding.EncodeToString(signature)
	v.Proof["proofPurpose"] = "assertionMethod"
	v.Proof["created"] = time.Now()
	return v, err
}

func (v *VerifiableCredential) VerifyVC(did string) error {
	var proofValue string
	pubKey, privateKey, err := getSubKey(did)
	if err != nil {
		return err
	}
	vc := VerifiableCredential{}
	vc.Id = v.Id
	vc.Issuer = v.Issuer
	vc.IssuanceDate = v.IssuanceDate
	vc.ExpirationDate = v.ExpirationDate
	vc.Context = v.Context
	vc.CredType = v.CredType

	vc.CredentialSubject = v.CredentialSubject
	vc.Proof = v.Proof
	if _, ok := vc.Proof["proofValue"]; ok {
		proofValue = v.Proof["proofValue"].(string)
		delete(vc.Proof, "proofValue")
		delete(vc.Proof, "proofPurpose")
		delete(vc.Proof, "created")
	}
	vcJson, err := json.Marshal(vc)
	if err != nil {
		return err
	}
	sig, err := base64.StdEncoding.DecodeString(proofValue)
	if err != nil {
		return err
	}
	pubkey, _ := decodeKeys(pubKey, privateKey)
	if !ed25519.Verify(pubkey, vcJson, []byte(sig)) {
		return errors.New("signature verification failed")
	}
	return nil
}

func (v *VerifiableCredential) GenKey(subjectID string) {
	generateSubEd25519Key(subjectID)
}
