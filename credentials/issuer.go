/*
 * Copyright (c) 2022 Huawei Technology Duesseldorf GmbH. 2022. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Contributors:
 *   Huawei Technologies Duesseldorf GmbH
 *   Security Forge
 */

package verifiablecredential

import (
	"bufio"
	"crypto/rand"
	"encoding/base64"
	"os"
	"strconv"
	"strings"

	"github.com/coinbase/kryptology/pkg/ted25519/ted25519"
)

var validSignatures [2]string = [2]string{"Ed25519", "bbs"}

func signer(mechanism string, value, privKey []byte) ([]byte, error) {
	//check if input is a valid mechanism
	if mechanism == "Ed255192020signature" {
		signature, err := ted25519.Sign(privKey, value)
		return signature, err
	}

	return []byte(""), nil
}

func verify(mechanism string, msg, signature, pubKey []byte) error {
	switch mechanism {
	case "Ed255192020signature":
		_, err := ted25519.Verify(pubKey, msg, signature)
		if err != nil {
			return err
		}
	}
	return nil
}

func generateEd25519Key(n int) {
	keyFile, err := os.Create("keyfile.txt")
	check(err)
	defer keyFile.Close()
	for i := 0; i < n; i++ {
		pk, sk, _ := ted25519.GenerateKey(rand.Reader)
		pkStr := base64.StdEncoding.EncodeToString(pk)
		skStr := base64.StdEncoding.EncodeToString(sk)
		_, err := keyFile.WriteString(strconv.Itoa(i) + ":" + pkStr + ":" + skStr + "\n")
		check(err)
	}
}

func generateSubEd25519Key(subjectID string) {
	keyFile, err := os.Create("keyfileSub.txt")
	check(err)
	defer keyFile.Close()

	pk, sk, _ := ted25519.GenerateKey(rand.Reader)
	pkStr := base64.StdEncoding.EncodeToString(pk)
	skStr := base64.StdEncoding.EncodeToString(sk)
	_, err = keyFile.WriteString(subjectID + ":" + pkStr + ":" + skStr + "\n")
	check(err)
}

// This is a local function to get the private key
func getKey(did string) (string, string, error) {
	fileDp, err := os.Open("keyfile.txt")
	check(err)
	defer fileDp.Close()
	scanner := bufio.NewScanner(fileDp)
	scanner.Split(bufio.ScanLines)
	var keys []string

	for scanner.Scan() {
		keys = append(keys, scanner.Text())
	}
	i, err := strconv.Atoi(did)
	check(err)
	if keys[i] != "" {
		key := strings.Split(keys[i], ":")
		return key[1], key[2], nil
	}
	return "", "", err
}

func getSubKey(subjectID string) (string, string, error) {
	fileDp, err := os.Open("keyfileSub.txt")
	check(err)
	defer fileDp.Close()
	scanner := bufio.NewScanner(fileDp)
	scanner.Split(bufio.ScanLines)
	var keys []string

	for scanner.Scan() {
		keys = append(keys, scanner.Text())
	}
	check(err)

	for _, key := range keys {
		value := strings.Split(key, ":")
		if value[0] == subjectID {
			return value[1], value[2], nil
		}
	}
	return "", "", err
}

func resolveKey(did string) []byte {
	// This method need to be implemented based on the verifiable data registry
	// for testing purpose, we will read from the file
	return []byte("test")
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func decodeKeys(pubKey, privKey string) ([]byte, []byte) {
	pubDecoded, err := base64.StdEncoding.DecodeString(pubKey)
	check(err)
	privDecoded, err := base64.StdEncoding.DecodeString(privKey)
	check(err)
	return pubDecoded, privDecoded
}
