/*
 * Copyright (c) 2022 Huawei Technology Duesseldorf GmbH. 2022. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Contributors:
 *   Huawei Technologies Duesseldorf GmbH
 *   Security Forge
 */

package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync"
	"text/template"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	credential "gitlab.com/uconpublic/credential-bridge/credentials"
	"gitlab.com/uconpublic/credential-bridge/pep"
	pb "gitlab.com/uconpublic/credential-bridge/proto"
)

var vpDemo string

type Bridge struct {
	ctx      context.Context
	conf     *pep.Config
	papUcs   pb.UcsPapServiceClient
	pipUcs   pb.UcsPipServiceClient
	pepUcs   pb.UcsServiceClient
	channels map[string]*pb.UcsService_UsageClient
}

type CredentialData struct {
	Credential    string
	Success       bool
	PolicySuccess bool
}

var fHost *string = flag.String("host", "localhost", "The server hostname or ip address")
var fPort *uint = flag.Uint("port", 10799, "The server port")

func main() {
	log.Printf("Credential Bridge Started!!!\n")
	log.Printf("Please visit URL localhost:8080 to configure the Bridge")
	http.HandleFunc("/", start)
	fs := http.FileServer(http.Dir("static/assets/"))
	http.Handle("/static/assets/", http.StripPrefix("/static/assets", fs))
	http.ListenAndServe(":8080", nil)
}

// need to build the demo interface
func start(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("templates/vc.html"))
	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}

	flag.Parse()
	conf := &pep.Config{
		Host: *fHost,
		Port: uint16(*fPort),
	}
	opts := []grpc.DialOption{grpc.WithBlock(), grpc.WithTimeout(10 * time.Second), grpc.WithInsecure()}
	conn, err := grpc.Dial(conf.Address(), opts...)

	if err != nil {
		return
	}

	bridge := &Bridge{
		conf:     conf,
		ctx:      context.Background(),
		papUcs:   pb.NewUcsPapServiceClient(conn),
		pipUcs:   pb.NewUcsPipServiceClient(conn),
		pepUcs:   pb.NewUcsServiceClient(conn),
		channels: make(map[string]*pb.UcsService_UsageClient),
	}

	// reading policy files
	attributes, err := getAttributes("attributes.json")
	if err != nil {
		log.Errorf("Error getting attributes")
	}

	if r.FormValue("policy") != "" {
		// installing a new policy
		bridge.createPolicy(r.FormValue("policy"))
		data := CredentialData{PolicySuccess: true}
		if err != nil {
			data = CredentialData{PolicySuccess: false}
		}
		tmpl.Execute(w, data)

	} else {

		// fetching user submitted credential and converting into vc struct. This is
		// customized now, we need to generalize it based on the context fetched.
		vcData := r.FormValue("vc")
		if vcData == "" {
			data := CredentialData{Success: false}
			tmpl.Execute(w, data)
		}

		// convert the user obtained VC into go struct
		vc := credential.VerifiableCredential{}
		vc = *vc.Create(string(vcData))
		bridge.createAttributeSet(attributes)

		// policy evaluation
		var wg sync.WaitGroup
		bridgeUsageHandler(conf, attributes, &wg, vc)
		wg.Wait()

		data := CredentialData{
			Credential: vpDemo,
			Success:    true,
		}
		log.Printf("Data:%s", data)
		tmpl.Execute(w, data)
	}
}

func bridgeUsageHandler(conf *pep.Config, attributes []*pb.Attribute, wg *sync.WaitGroup, vc credential.VerifiableCredential) {
	wg.Add(1)
	go func() {
		// invoking the policy evaluation engine
		err := pep.CredentialMap(conf, attributes, buildingVP, vc)
		if err != nil {
			log.Warn(err)
		}
		wg.Done()
	}()
}

// logic for credential mapping goes here.
func buildingVP(bridgeHandler *pep.BridgeHandler) {
	select {
	case <-bridgeHandler.Revoke():
		log.Info("Closing the Credentials Bridge!")
		bridgeHandler.Close()
		return
	default:
		// Initiate the Verifiable Presentation
		vp := credential.InitVP("identityBridge2", "P1")
		// adding VC or tokens into the VP
		vp.Vc = append(vp.Vc, bridgeHandler.GetVC())

		// get the obligations after policy evaluation
		for _, value := range bridgeHandler.Obligatons() {
			updateVP(vp, value)
		}
		// this could obtained from the federation
		vp.Proof["ProofType"] = "Ed255192020signature"
		// creating the JSON-LD while
		vpValue, err := vp.BuildLD("identityBridge2")
		if err != nil {
			log.Println(err)
		}
		vpJson, err := json.Marshal(vpValue)
		if err != nil {
			return
		}
		vpDemo = string(vpJson)
	}
	bridgeHandler.Close()
}

// This method helps with credential mapping
func updateVP(vp *credential.VerifiablePresentation, instructions []*pb.Instruction) {
	for _, instruction := range instructions {
		contentType := instruction.Assignments["contentType"]
		if contentType == "claims" {
			err := updateClaims(vp, instruction.Assignments["claims"])
			if err != nil {
				return
			}
		}
		if contentType == "externalInfo" {
			log.Printf("Inside external Infor")
			additionalInfo, err := updateInfo(vp, instruction)
			log.Printf("Additional Info: %s", &additionalInfo)
			if err != nil {
				return
			}
			vp.Vc = append(vp.Vc, additionalInfo)
		}
		if contentType == "gaia-x" {
			vp.PresentationMeta["gaia-x"] = instruction.Assignments["gx_trust_status"]
		}
		if contentType == "metadata" {
			vp.Context = append(vp.Context, strings.Split(instruction.Assignments["context"], ",")...)
			vp.CredType = append(vp.CredType, strings.Split(instruction.Assignments["type"], ",")...)
		}
		if contentType == "proof" {
			vp.Proof["ProofType"] = instruction.Assignments["ProofType"]
			vp.Proof["proofPurpose"] = instruction.Assignments["proofPurpose"]
			vp.Proof["verificationMethod"] = instruction.Assignments["verificationMethod"]
		}
	}
}

// helper method to update the claims in a VP
func updateClaims(vp *credential.VerifiablePresentation, claims string) error {
	if claims == "" {
		return errors.New("claim is empty")
	}
	var claimJson map[string]interface{}
	json.Unmarshal([]byte(claims), &claimJson)
	for key, value := range claimJson {
		vp.PresentationMeta[key] = value
	}
	return nil
}

// helper method to obtained third-party credential and update the VP
func updateInfo(vp *credential.VerifiablePresentation, instruction *pb.Instruction) (credential.VerifiableCredential, error) {
	//var externalInfo map[string]string
	// fetch zk proof from a trust authority.
	vc := &credential.VerifiableCredential{}
	bytesCred, err := os.ReadFile("attributes_remote.json")
	if err != nil {
		log.Printf("Error1:%s", err)
		return *vc, err
	}

	// convert the user obtained VC into go struct
	vc = vc.Create(string(bytesCred))
	vc.Proof["proofPurpose"] = instruction.Assignments["data"]
	vc.Proof["endpoint"] = instruction.Assignments["endpoint"]
	vc.Proof["zkProof"] = vc.Proof["proofValue"]
	vc.Proof["externalVerifier"] = vc.Proof["verificationMethod"]
	v, err := vc.BuildLD("identityBridge2")
	if err != nil {
		log.Printf("Error:%s", err)
		return *vc, nil
	}
	return *v, nil
}

// helper function to obtain additional proofs from trusted
// authority.
func getProof(sourceURL string) (string, error) {
	if sourceURL == "" {
		return "", errors.New("info is empty")
	}
	//mock for fetching credentials from remote authority
	bytesCred, err := os.ReadFile("remote_attribute.json")
	if err != nil {
		return "", err
	}

	// convert the user obtained VC into go struct
	vc := credential.VerifiableCredential{}
	vc = *vc.Create(string(bytesCred))

	return string(bytesCred), nil
}

// fetching the attributes from an external file.
func getAttributes(attributesFile string) ([]*pb.Attribute, error) {
	jsonAttr, err := os.Open(attributesFile)
	if err != nil {
		log.Printf("%s", err)
	}
	defer jsonAttr.Close()
	byteValue, _ := ioutil.ReadAll(jsonAttr)
	var attributes struct {
		Attributes []pb.Attribute
	}

	err = json.Unmarshal(byteValue, &attributes.Attributes)
	if err != nil {
		log.Printf("Parsing Error:%s", err)
	}
	var pbAttributes []*pb.Attribute
	for _, attr := range attributes.Attributes {
		pbAttributes = append(pbAttributes, &pb.Attribute{
			Id:       attr.Id,
			Category: attr.Category,
			DataType: attr.DataType,
			Issuer:   attr.Issuer,
			Values:   attr.Values,
		})
	}
	return pbAttributes, nil
}

// installing the policy to the credential-bridge
func (u *Bridge) createPolicy(policy string) string {
	log.Printf("Policy Received: %s", policy)
	resp, err := u.papUcs.CreatePolicy(u.ctx, &pb.CreatePolicyRequest{Policy: policy})
	if err != nil {
		log.Printf("Issue in uploading policy: %+v", err)
	}
	return resp.Id
}

// deleting the policies from the credential bridge
func (u *Bridge) deletePolicy(policyId string) {
	log.Printf("Policy received: %s", policyId)
	resp, err := u.papUcs.DeletePolicy(u.ctx, &pb.DeletePolicyRequest{Id: policyId})
	if err != nil {
		log.Printf("Error occurred in removing policy: %+v", err)
	} else {
		log.Printf("Policy removed: %s:%s", policyId, resp)
	}
}

// helper function to update the attributes in the policy engine
func (u *Bridge) createAttributeSet(attributes []*pb.Attribute) {
	log.Printf("CreateAttribute received: %s", attributes)

	for _, attribute := range attributes {
		resp, err := u.pipUcs.PipCreate(u.ctx, &pb.PipCreateRequest{Attribute: attribute})
		if err != nil {
			log.Printf("Error occurred in creating attribute: %+v", err)
		} else {
			log.Printf("Attribute created: %s:%s", attribute.Id, resp)
		}
	}
}

// helper function to delete the attributes
func (u *Bridge) deleteAttribute(attributedId string) {
	_, err := u.pipUcs.PipDelete(u.ctx, &pb.PipDeleteRequest{Id: attributedId})
	if err != nil {
		log.Printf("Error occurred in creating attribute: %+v", err)
	} else {
		log.Printf("Attribute deleted: %s", attributedId)
	}
}

// Fetch attributes from the credentials to initiate policy evaluation
func (u *Bridge) fetchAttributes(vc credential.VerifiableCredential, attributes []*pb.Attribute) []*pb.Attribute {
	for key, value := range vc.CredentialSubject {
		attributes = append(attributes, &pb.Attribute{
			Id:       "Attributes." + key,
			Category: "",
			Issuer:   "",
			DataType: "string",
			Values:   []string{value.(string)},
		})
	}

	return attributes
}
